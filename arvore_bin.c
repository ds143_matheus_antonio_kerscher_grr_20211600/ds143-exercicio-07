#include <stdio.h>
#include <stdlib.h>

typedef struct no
{
  int conteudo;
  struct no *esquerda, *direita;
} No;

typedef struct
{
  No *raiz;
  int tam;
} ArvB;

void inserirDireita(No *no, int valor);
void inserirEsquerda(No *no, int valor);
No *inserir(No *raiz, int valor);
int tamanho(No *raiz);
int buscar(No *raiz, int chave);
void imprimir(No *raiz);
No *remover(No *raiz, int chave);

int main()
{
  int op, valor;

  ArvB arv;
  arv.raiz = NULL;

  No *raiz = NULL;

  do
  {
    printf("---------- MENU ----------\n");
    printf("\n0 - Sair\n1 - Inserir\n2 - Imprimir\n3 - Buscar\n4 - Remover\n=> ");
    scanf("%d", &op);

    switch (op)
    {
    case 0:
      printf("\nSaindo...\n");
      break;
    case 1:
      printf("Qual valor deseja inserir? \n=> ");
      scanf("%d", &valor);
      raiz = inserir(raiz, valor);
      break;
    case 2:
      printf("\nImpressao da arvore binaria:\n");
      imprimir(raiz);
      printf("\n");
      printf("Tamanho: %d\n", tamanho(raiz));
      break;
    case 3:
      printf("Qual valor deseja buscar? \n=> ");
      scanf("%d", &valor);
      printf("Resultado da busca: %d\n", buscar(raiz, valor));
      break;
    case 4:
      printf("Qual valor deseja remover? \n=> ");
      scanf("%d", &valor);
      raiz = remover(raiz, valor);
      break;
    default:
      printf("\nOpcao invalida...\n");
    }
  } while (op != 0);
}

void inserirEsquerda(No *no, int valor)
{
  if (no->esquerda == NULL)
  {
    No *novo = (No *)malloc(sizeof(No));
    novo->conteudo = valor;
    novo->esquerda = NULL;
    novo->direita = NULL;
    no->esquerda = novo;
  }
  else
  {
    if (valor < no->esquerda->conteudo)
      inserirEsquerda(no->esquerda, valor);
    if (valor > no->esquerda->conteudo)
      inserirDireita(no->esquerda, valor);
  }
}

void inserirDireita(No *no, int valor)
{
  if (no->direita == NULL)
  {
    No *novo = (No *)malloc(sizeof(No));
    novo->conteudo = valor;
    novo->esquerda = NULL;
    novo->direita = NULL;
    no->direita = novo;
  }
  else
  {
    if (valor > no->direita->conteudo)
      inserirDireita(no->direita, valor);
    if (valor < no->direita->conteudo)
      inserirEsquerda(no->direita, valor);
  }
}

No *inserir(No *raiz, int valor)
{
  if (raiz == NULL)
  {
    No *novo = (No *)malloc(sizeof(No));
    novo->conteudo = valor;
    novo->esquerda = NULL;
    novo->direita = NULL;
    return novo;
  }
  else
  {
    if (valor < raiz->conteudo)
      raiz->esquerda = inserirNovaVersao(raiz->esquerda, valor);
    if (valor > raiz->conteudo)
      raiz->direita = inserirNovaVersao(raiz->direita, valor);
    return raiz;
  }
}

int tamanho(No *raiz)
{
  if (raiz == NULL)
    return 0;
  else
    return 1 + tamanho(raiz->esquerda) + tamanho(raiz->direita);
}

int buscar(No *raiz, int chave)
{
  if (raiz == NULL)
  {
    return 0;
  }
  else
  {
    if (raiz->conteudo == chave)
      return 1;
    else
    {
      if (chave < raiz->conteudo)
        return buscar(raiz->esquerda, chave);
      else
        return buscar(raiz->direita, chave);
    }
  }
}

void imprimir(No *raiz)
{
  if (raiz != NULL)
  {
    imprimir(raiz->esquerda);
    printf("%d ", raiz->conteudo);
    imprimir(raiz->direita);
  }
}

No *remover(No *raiz, int chave)
{
  if (raiz == NULL)
  {
    printf("Valor nao encontrado!\n");
    return NULL;
  }
  else
  {
    if (raiz->conteudo == chave)
    {

      if (raiz->esquerda == NULL && raiz->direita == NULL)
      {
        free(raiz);
        return NULL;
      }
      else
      {
        if (raiz->esquerda == NULL || raiz->direita == NULL)
        {
          No *aux;
          if (raiz->esquerda != NULL)
            aux = raiz->esquerda;
          else
            aux = raiz->direita;
          free(raiz);
          return aux;
        }
        else
        {
          No *aux = raiz->esquerda;
          while (aux->direita != NULL)
            aux = aux->direita;
          raiz->conteudo = aux->conteudo;
          aux->conteudo = chave;
          raiz->esquerda = remover(raiz->esquerda, chave);
          return raiz;
        }
      }
    }
    else
    {
      if (chave < raiz->conteudo)
        raiz->esquerda = remover(raiz->esquerda, chave);
      else
        raiz->direita = remover(raiz->direita, chave);
      return raiz;
    }
  }
}